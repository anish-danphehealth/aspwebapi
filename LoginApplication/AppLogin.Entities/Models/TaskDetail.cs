﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLogin.Entities.Models
{
    public class TaskDetail
    {
        [Key]
        public int TaskId { get; set; }
        public string Detail { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? Done { get; set; }
        public bool IsActive { get; set; }
    }
}

namespace AppLogin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newcommitwithUserfieldremoved : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TaskDetails", "CreatedBy", "dbo.AspNetUsers");
            DropIndex("dbo.TaskDetails", new[] { "CreatedBy" });
            AlterColumn("dbo.TaskDetails", "CreatedBy", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskDetails", "CreatedBy", c => c.String(maxLength: 128));
            CreateIndex("dbo.TaskDetails", "CreatedBy");
            AddForeignKey("dbo.TaskDetails", "CreatedBy", "dbo.AspNetUsers", "Id");
        }
    }
}

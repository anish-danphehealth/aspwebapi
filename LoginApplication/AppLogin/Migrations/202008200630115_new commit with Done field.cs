namespace AppLogin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newcommitwithDonefield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskDetails", "Done", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskDetails", "Done");
        }
    }
}

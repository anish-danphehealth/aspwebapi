// <auto-generated />
namespace AppLogin.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ClientModelAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ClientModelAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "202008191034013_ClientModelAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace AppLogin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newcommit : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskDetails", "CreatedBy", c => c.String(maxLength: 128));
            AlterColumn("dbo.TaskDetails", "CreatedOn", c => c.DateTime());
            CreateIndex("dbo.TaskDetails", "CreatedBy");
            AddForeignKey("dbo.TaskDetails", "CreatedBy", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaskDetails", "CreatedBy", "dbo.AspNetUsers");
            DropIndex("dbo.TaskDetails", new[] { "CreatedBy" });
            AlterColumn("dbo.TaskDetails", "CreatedOn", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TaskDetails", "CreatedBy", c => c.String());
        }
    }
}

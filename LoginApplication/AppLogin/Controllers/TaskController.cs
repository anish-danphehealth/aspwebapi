﻿using AppLogin.Entities.Models;
using AppLogin.Repos;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;

namespace AppLogin.Controllers
{
    
    [RoutePrefix("api/Task")]
    public class TaskController : ApiController
    {        
        [HttpGet]
        [Authorize(Roles = "USER")]
        [Route("TaskList")]
        public object GetTaskList()
        {
            try
            {
                ApplicationDbContext dbContext = new ApplicationDbContext();
                string userName;
                userName = User.Identity.Name;
                var id = dbContext.Users.Where(u => u.UserName == userName).Select(v => v.Id).FirstOrDefault();
                var res = (from task in dbContext.TaskDetails
                           join user in dbContext.Users on task.CreatedBy equals user.Id
                           where user.UserName == userName && task.IsActive == true
                           select task).ToList();
                return new { Status = true, Data = res, Message = "Data rendered" };
            }
            catch (Exception ex)
            {
                return new { Status = false, Message = ex.InnerException };
            }           
        }

        [HttpPost]
        [Authorize(Roles = "USER")]
        [Route("CreateTask")]
        public object CreateTask(TaskDetail task)
        {
            ApplicationDbContext dbContext = new ApplicationDbContext();
            try
            {
                task.CreatedBy = User.Identity.GetUserId();
                task.CreatedOn = System.DateTime.Now;
                dbContext.TaskDetails.Add(task);
                dbContext.SaveChanges();
                return new { Status = true, Data = task, Message = "Data added successfully." };
            }
            catch (Exception ex)
            {
                return new { Status = false, Message = ex.InnerException };
            }
            
        }

        [HttpPut]
        [Authorize(Roles = "USER")]
        [Route("UpdateTask")]
        public object UpdateTask(TaskDetail task)
        {
            try
            {
                ApplicationDbContext dbContext = new ApplicationDbContext();
                
                var taskToEdit = dbContext.TaskDetails.Where(t => t.TaskId == task.TaskId).FirstOrDefault();
                task.ModifiedBy = taskToEdit.ModifiedBy = User.Identity.GetUserId();
                task.ModifiedOn = taskToEdit.ModifiedOn = System.DateTime.Now;
                taskToEdit.Detail = task.Detail;
                taskToEdit.IsActive = task.IsActive;
                dbContext.Entry(taskToEdit).Property(p=>p.CreatedBy).IsModified = false;
                dbContext.Entry(taskToEdit).Property(p=>p.CreatedOn).IsModified = false;
                dbContext.SaveChanges();
                return new { Status = true, Data = taskToEdit, Message = "Data Updated successfully." };
            }
            catch (Exception ex)
            {
                return new { Status = false, Message = ex.InnerException };
            }
        }

        [HttpGet]
        [Authorize(Roles = "ADMINISTRATOR")]
        [Route("AllTasks")]
        public object GetAllTask()
        {
            try
            {
                ApplicationDbContext dbContext = new ApplicationDbContext();
                var allUsers = dbContext.Users.AsQueryable();
                var taskList = dbContext.TaskDetails.GroupBy(c => c.CreatedBy, (key, val) => new
                {
                    id = key,
                    tasks = val
                }).AsQueryable();

                var result = (from u in allUsers
                              join t in taskList on u.Id equals t.id
                              select new
                              {
                                  firstName = u.FirstName,
                                  lastName = u.LastName,
                                  tasks = t.tasks
                              }).ToList();

                //var res = dbContext.TaskDetails.Where(t => t.IsActive == true).ToList();
                return new { Status = true, Data = result, Message = "Data Rendered successfully." };
            }
            catch (Exception ex)
            {
                return new { Status = false,  Message = ex.InnerException };
            }
        }
        
    }
}
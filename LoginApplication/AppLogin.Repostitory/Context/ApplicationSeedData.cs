﻿using AppLogin.Entities.Models;
using AppLogin.Repos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLogin.Repos
{
    public class ApplicationSeedData
    {
        private ApplicationDbContext _context;

        public ApplicationSeedData(ApplicationDbContext context)
        {
            _context = context;
        }

        public async void SeedDefaultDatas()
        {
            if (_context.Clients.Count() == 0)
            {
                _context.Clients.AddRange(BuildClientsList());
                await _context.SaveChangesAsync();
            }

            var roleStore = new RoleStore<IdentityRole>(_context);

            if (!_context.Roles.Any(r => r.Name == "USER"))
                await roleStore.CreateAsync(new IdentityRole { Name = "USER" });

            if (!_context.Roles.Any(r => r.Name == "ADMINISTRATOR"))
                await roleStore.CreateAsync(new IdentityRole { Name = "ADMINISTRATOR" });

            string adminEmailAddress = "helloworld@lorem.com";
            //Adminstrator
            var user = new ApplicationUser
            {
                UserName = adminEmailAddress,
                Email = adminEmailAddress,
                FirstName = "Anish",
                LastName = "Bhattarai",
                Organization = "GPO Net",
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            if (!_context.Users.Any(a => a.UserName == adminEmailAddress))
            {
                var password = new PasswordHasher();
                var hashed = password.HashPassword("helloworld");
                user.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "ADMINISTRATOR");
                _context.SaveChanges();
            }
        }

        private List<Client> BuildClientsList()
        {
            List<Client> ClientsList = new List<Client>
            {
                new Client
                {
                    Id = "AppOne",
                    Name="Hello Bob",
                    IsActive = true
                },
                new Client
                {
                    Id = "AppTwo",
                    Name="Chill on the Beach",
                    IsActive = true
                }
            };
            return ClientsList;
        }
    }
}
